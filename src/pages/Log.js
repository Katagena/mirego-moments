import React, { Component } from 'react';
import Header from 'components/Header'
import { withCookies } from 'react-cookie';
import FormLog from 'components/_parts/_FormLog'

class LogPage extends Component {
  constructor(props) {
      super(props);
      const { isRegister } = this.props;      
      const type = isRegister ? "Register" : "Login"
      this.state = {type};
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isRegister } = nextProps;    
    let type = isRegister ? "Register" : "Login"
    document.title = `Mirego - ${type}`;
    return ({type});
  }
  
  componentDidMount(){
    const { cookies, history } = this.props; 
    if (cookies.get('auth'))
      history.push("/")
  }

  render(){
    const { isRegister } = this.props;
    const { type } = this.state;
    return ( 
      <>
        <Header breads={[{ label: 'Mirego Moments', url: '/' }, { label: type, url: '' }]}/>
        <FormLog register={isRegister}/>
      </>
    )
  }
}

export default withCookies(LogPage);