import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import PhotoContainer from 'components/PhotoContainer';
import Header from 'components/Header'
import { mergeLikeHelper } from 'helpers/MergeLikeHelper'
import { requestLoginService } from 'helpers/RequestLoginService'

class ViewerPage extends Component {
  constructor(props) {
      super(props);
      this.state = {
        photo: null,
      }
  }

  componentDidMount() {
    const { history, cookies, } = this.props;
    const id = this.props.match.params.id; 
    requestLoginService(history, cookies, id).then(result => {
      const photo = mergeLikeHelper.withInt(...result)
      document.title = `Mirego - ${photo.description}`;
      this.setState({
        photo, loaded: true
      })
    });
  }

  render(){
    const { photo } = this.state;
    return ( 
        <>
          { photo && 
          <>
            <Header breads={[{label: 'Mirego Moments', url: '/'}, {label: photo.description, url: ''}]}/>
            <div className="viewer-page">
              <PhotoContainer key={photo.id} details={photo}/>
            </div>
          </>
          }
        </>
    )
  }
}
     
export default withCookies(ViewerPage);