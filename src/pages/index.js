import HomePage from './Home';
import ViewerPage from './Viewer';
import ErrorPage from './Error';
import LogPage from './Log';
import StatPage from './Stat';

export {
  HomePage,
  ViewerPage,
  ErrorPage,
  LogPage,
  StatPage
}