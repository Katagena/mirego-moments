import React, { Component } from 'react';
import Header from 'components/Header';
import { Icon } from 'antd';
import Histogramme from 'components/graphs/Histogramme';
import LikeOverTime from 'components/graphs/LikeOverTime';
import TopPoster from 'components/graphs/TopPoster';
import { withCookies } from 'react-cookie';
import { miregoService } from 'services/mirego.service';

class StatPage extends Component {
  constructor(props){
    super(props);
    this.state = {
      datas: []
    }
    this.refGraph1 = React.createRef(); 
    this.refGraph2 = React.createRef(); 
    this.refGraph3 = React.createRef(); 
  }

  componentDidMount() {
    document.title = 'Mirego - Statistiques';
    this.loadDatas().then(datas => this.setState({datas}));
  }

  // charge les photos et merge les likes
  loadDatas = async () => {
    const { history, cookies } = this.props
    try {
      const datas = await miregoService.get(null, null);
      return datas
    } catch (error) {
      if (error.result.status === 401){
        cookies.remove('auth')
        history.push("/login")
      }else{
        history.push("/error")
      }
    }
  }

  handleClick = () => {
    this.refGraph1.current.handleClick()
    this.refGraph2.current.handleClick()
    this.refGraph3.current.handleClick()
  }
  
  render(){
    const { datas } = this.state;
    return ( 
        <>
            <Header breads={[{label: 'Mirego Moments', url: '/'}, {label: 'Statistiques', url: ''}]}/>
            <button onClick={this.handleClick}><Icon type="fast-backward" /> More... </button>
            { datas.length > 0 && <Histogramme ref={this.refGraph1} datas={datas} />}
            <button onClick={this.handleClick}><Icon type="fast-backward" /> More... </button>
            { datas.length > 0 && <LikeOverTime ref={this.refGraph2} datas={datas} />}
            <button onClick={this.handleClick}><Icon type="fast-backward" /> More... </button>
            { datas.length > 0 && <TopPoster ref={this.refGraph3} datas={datas} />}
        </>
    )
  }
}
     
export default withCookies(StatPage);