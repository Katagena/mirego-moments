import React, { Fragment, Component } from 'react';
import PhotosList from 'components/PhotosList';
import { withCookies } from 'react-cookie';
import Header from 'components/Header'
import { mergeLikeHelper } from 'helpers/MergeLikeHelper'
import { requestLoginService } from 'helpers/RequestLoginService'

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      maxDate: null,
    }
    this.processing = false;
  }

  componentDidMount() {
    document.title = 'Mirego - Moments';
    window.addEventListener('scroll',this.handleScroll);
    this.loadPhotos();
  }

  componentWillUnmount(){
    window.removeEventListener('scroll',this.handleScroll);
  }
  
  // charge les photos et merge les likes
  loadPhotos = () => {
    const { history, cookies } = this.props;
    const { id, maxDate } = this.state; 
    let photos = [...this.state.photos]
    requestLoginService(history, cookies, id, maxDate).then(result => {
      result[2] = photos.concat(result[2])
      photos = mergeLikeHelper.withArray(...result)
      this.processing = false;
      this.setState({
        photos,
        maxDate: photos[photos.length - 1].created_at
      })
    })
  }

  // infinite loop
  handleScroll = () => {
    if (!this.processing && (window.scrollY) + 2000 >= document.getElementsByClassName("home-page")[0].offsetHeight) {
      this.loadPhotos();
      this.processing = true;
    }
  }

  render() {
    const { photos } = this.state
    return (
      <Fragment>
        <Header breads={[{ label: 'Mirego Moments', url: '' }]}/>
        <div className="home-page main-content">
          <PhotosList onScroll={this.handleScroll} photos={photos}/>
        </div>
      </Fragment>
    )
  }
}
     
export default withCookies(HomePage);