import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import {
    HomePage, ViewerPage, ErrorPage, LogPage, StatPage,
} from './pages';

class AppRouter extends Component {
  render(){
    return(
      <Router>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/home" component={HomePage} />
          <Route exact path="/error" component={ErrorPage} />
          <Route exact path="/register" render={(props) => <LogPage {...props} isRegister={true} />} />
          <Route exact path="/login" render={(props) => <LogPage {...props} isRegister={false} />}/>
          <Route exact path="/stat" component={StatPage}/>
          <Route path="/:id" component={ViewerPage} />
        </Switch>
      </Router>
    )
  }
}

export default AppRouter;