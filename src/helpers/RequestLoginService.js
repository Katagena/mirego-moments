import { miregoService } from 'services/mirego.service';
import { loginService } from 'services/login.service';

export const requestLoginService = async (history, cookies, id, maxDate = null) => {
  const auth = cookies.get("auth");
  let liked = []
  try {
    const photo = await miregoService.get(id, maxDate)
    const likes = await loginService.getLikes(id)
    if (auth) {
      liked = await loginService.getUserLikes(auth.token, auth.id, id)
    }
    return await Promise.all([likes, liked, photo])
  } catch (error) {
    if (error.result && error.result.status === 401) {
      cookies.remove('auth')
      history.push("/login")
    } else {
      history.push("/error")
    }
  }
}