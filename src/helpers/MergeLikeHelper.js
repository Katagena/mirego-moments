export const mergeLikeHelper = {
  withArray, withInt
}

function withArray(likes, likeds, photos) {
  let likesHash = {};
  likes.map( like => likesHash[like.id] = {'likes': like.like});
  likeds.map( liked => likesHash[liked.photo_id] = {...likesHash[liked.photo_id], ...{'isLiked': true}});
  for(let photo_index in photos){
    const id = photos[[photo_index]].id
    if ( likesHash[id] ){
      photos[photo_index].liked = likesHash[id].isLiked ? true : false;
      photos[photo_index].emotions.love = photos[photo_index].emotions.love + likesHash[id].likes;
    }
  }
  return photos;
}

function withInt(likes, liked, photo) {
  photo.emotions.love = photo.emotions.love + likes.like
  photo.liked = liked.like ? true : false
  return photo;
}