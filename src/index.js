import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from 'router';
import { CookiesProvider } from 'react-cookie';
import ErrorBoundary from 'components/ErrorBoundary'
import 'antd/dist/antd.css';
import 'assets/scss/index.scss';

class App extends React.Component {
    render(){
        return(
          <ErrorBoundary>
            <CookiesProvider>
              <AppRouter />
            </CookiesProvider>
          </ErrorBoundary>
        )
    }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement)