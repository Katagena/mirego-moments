import { API_MIREGO_INDEX, API_MIREGO_SHOW } from '../constants/api'

export const miregoService = {
  get
}

function get(id = null, since = null) {
  let URL = id ? `${API_MIREGO_SHOW}/${id}` : API_MIREGO_INDEX
  URL = since ? `${URL}?since=${since}` : URL
  return new Promise((resolve, reject) => {
      fetch(URL)
      .then(result => { 
        if (result.status >= 200 && result.status < 300) {
          return resolve(result.json()) 
        } else {
          var error = new Error(result.statusText || result.status)
          error.result = result
          reject(error)
        }
      })
      .catch(error => { reject(error) });
  })
}