import { API_LOGIN } from '../constants/api'

export const loginService = {
  log,
  postLike,
  getLikes,
  getUserLikes
}

const promise = (URL, postInfo = null) => new Promise((resolve, reject) => {
  fetch(URL, postInfo)
  .then(result => { 
    if (result.status >= 200 && result.status < 300) {
      return resolve(result.json()) 
    } else {
      var error = new Error(result.statusText || result.status)
      error.result = result
      reject(error)
    }
  })
  .catch(error => { reject(error) });
})

function log(username, password, register = false) {
  let URL;
  if (register)
  URL = `${API_LOGIN}/register`
  else
  URL = `${API_LOGIN}/login`
  
  const postInfo = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username,
      password
    })
  }
  return promise(URL, postInfo);
}

function getLikes(photo_id = null){
  let URL = `${API_LOGIN}/likes/count`;
  URL = photo_id ? `${URL}/${photo_id}`: URL;
  return promise(URL);
}

function getUserLikes(token, user_id, photo_id = null){
  let URL = `${API_LOGIN}/likes/by_user/${user_id}`
  URL = photo_id ? `${URL}/by_photo/${photo_id}`: URL;
  const postInfo = {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`,
    }
  };
  return promise(URL, postInfo);
}

function postLike(user_id, photo_id, token) {
  let URL = `${API_LOGIN}/likes`
  const postInfo = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`,
    },
    body: JSON.stringify({
      user_id,
      photo_id
    })
  }
  return promise(URL, postInfo);
}