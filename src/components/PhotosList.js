import React, {Component} from "react";
import PictureContainer from 'components/PhotoContainer';

class PhotosList extends Component{
  render(){
      const { photos, handleScroll } = this.props
      return (
          <div className="photos-list" onScroll={handleScroll}>
          {photos.map(photo => <PictureContainer key={photo.id} details={photo} isList="true" />)}
          </div>
      )
  }
}

export default PhotosList