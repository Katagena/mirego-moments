import React, { Component } from "react";
import { miregoService } from 'services/mirego.service';
import * as d3 from "d3";

class TopPoster extends Component {
  constructor(props){
    super(props);
    const { datas } = this.props

    this.margin = {top: 20, right: 30, bottom: 50, left: 20};
    this.width = 1300 - this.margin.left - this.margin.right;
    this.height = 500 - this.margin.top - this.margin.bottom;
    this.maxDate = datas[datas.length -1].created_at

    this.updatedDatas = datas;
    this.svg = null;
    this.process = false;
    this.isPost = true;
  }

  loadDatas = () => {
    const _ = this;

    miregoService.get(null, _.maxDate).then(datas => {
      _.updatedDatas = [..._.updatedDatas, ...datas];
      _.maxDate = _.updatedDatas[_.updatedDatas.length -1].created_at
      _.updateChart();
    }).catch(error => console.debug(error));
  }

  handleClick = () => {
    if (!this.process){
      this.loadDatas();
      this.process = true; 
    } 
  }
    
  drawChart = () => {
    const _ = this
    const datas = this.updatedDatas;
    const node = _.node
    const { margin, width, height } = this;

    _.svg = d3.select(node)
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

    _.groupBars = _.svg.append('g').attr('class', 'group-bars');
    _.svg.append('g').attr('class', 'xAxis').attr('transform', 'translate(0, ' + height+ ')');
    _.svg.append('g').attr('class', 'yAxis');

    _.xScale = d3.scaleLinear()
    .range([0, width]);

    _.yScale = d3.scaleBand()
    .range([0, height])
    .padding(0.2);

    _.xScale.domain([0, d3.max(datas, d => d.post)]);
    _.yScale.domain(datas.map(d => d.id));

    _.yAxis = d3.axisRight(_.yScale)
    .tickFormat((d, i) => datas[i].user.full_name );

    _.xAxis = d3.axisBottom(_.xScale)
    .tickFormat(d3.format("d"));

    _.svg.select('.xAxis').transition().call(_.xAxis);
    _.svg.select('.yAxis').transition().call(_.yAxis);

     this.updateChart();
  };

  updateChart = () => {
    const _ = this;
    let datas = _.updatedDatas;

    const { height } = this;
    let topPoster = {};

    const type = this.isPost ? 'post' : 'like';

    datas.forEach((d) => { 
      if (!topPoster[d.user.id]){
        topPoster[d.user.id] = {id: d.user.id, post: 1, like: d.emotions.love, user: d.user}
      }else{
        topPoster[d.user.id]['post']++;
        topPoster[d.user.id]['like']+=d.emotions.love;
      }
    })

    datas = Object.keys(topPoster).map(i => topPoster[i])

    datas.sort((a,b) => b[type] - a[type]);

    _.xScale.domain([0, d3.max(datas, d => d[type])]);
    _.yScale.domain(datas.map(d => d.id));
    const nb_ticks = this.isPost ? d3.max(datas, d => d[type]) : d3.max(datas, d => d[type]/10)
    _.yAxis = d3.axisLeft(_.yScale)
    .tickFormat((d, i) => datas[i].user.full_name );
    _.xAxis = d3.axisBottom(_.xScale)
    .ticks(nb_ticks)
    .tickFormat(d3.format("d"))
    .tickSize(-height);

    _.svg.select('.xAxis').transition().call(_.xAxis);
    _.svg.select('.yAxis').transition().call(_.yAxis);

    _.bars = _.groupBars.selectAll('.bar').data(datas);

    _.bars.enter()
    .append("rect")
    .attr("class","bar")
    .attr("width", 0)
    .attr("height", _.yScale.bandwidth() )
    .attr("x", 0)
    .attr("y", d => _.yScale(d.id))
    .transition()
    .duration(1000)
    .attr("width", d => _.xScale(d[type]))

    _.bars.transition()
    .attr("width", d => _.xScale(d[type]))
    .attr("x", 0)
    .attr("height", _.yScale.bandwidth() )
    .attr("y", d => _.yScale(d.id))
    .on("end", () => this.process = false);

    d3.selectAll(".title")
      .text(this.isPost ? "Total Posts" : "Total Likes"); 
  }

  handleClickType = () => {
    this.isPost = !this.isPost;
    this.updateChart();
  }

  componentDidMount() {
    this.drawChart();
  }
        
  render(){
    return (
      <div className="top-poster">
        <button onClick={this.handleClickType}> Top Post / Top Liked </button>
        <header> Top Poster / Liked </header>
        <svg ref={node => this.node = node}>
          <text y="0" x="60" dy="1em" textAnchor="middle" className="title">fred</text>
        </svg>
      </div>
    )

  }
}

export default TopPoster