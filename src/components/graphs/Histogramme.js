import React, { Component } from "react";
import { Modal } from 'antd';
import PhotoContainer from 'components/PhotoContainer'
import { miregoService } from 'services/mirego.service';
import { BrowserRouter as Router } from 'react-router-dom';
import * as d3 from "d3";

class Histogramme extends Component {
  constructor(props){
    super(props);
    const { datas } = this.props

    this.margin = {top: 10, right: 30, bottom: 50, left: 20};
    this.width = 1300 - this.margin.left - this.margin.right;
    this.height = 500 - this.margin.top - this.margin.bottom;
    this.maxDate = datas[datas.length -1].created_at

    this.updatedDatas = datas;
    this.svg = null;
    this.process = false;
  }

  loadDatas = () => {
    const _ = this;

    d3.selectAll(".thumb.open")
    .attr('xlink:href', d => d.media.thumb)
    .attr("class", "thumb")
    .transition()
    .attr('width', 40)
    .attr('height', 40)
    .attr("x", d => _.xScale(new Date(d.created_at)))
    .attr("y", d => _.yScale(d.emotions.love))

    miregoService.get(null, _.maxDate).then(datas => {
      _.updatedDatas = [..._.updatedDatas, ...datas];
      _.maxDate = _.updatedDatas[_.updatedDatas.length -1].created_at
      _.updateChart();
    }).catch(error => console.debug(error));
  }

  handleClick = () => { 
    if (!this.process){
      this.loadDatas();
      this.process = true; 
    } 
  }
    
  drawChart = () => {
    const _ = this
    const datas = this.updatedDatas;
    const node = _.node
    const { margin, width, height } = this;

    _.svg = d3.select(node)
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

    _.svg.append('g').attr('class', 'xAxis').attr('transform', 'translate(0, ' + height+ ')');
    _.svg.append('g').attr('class', 'yAxis');
    _.groupThumbs = _.svg.append('g').attr('class', 'group-thumbs');

    _.xScale = d3.scaleTime()
    .range([0, width]);

    _.yScale = d3.scaleLinear()
    .range([height, 0]);

    _.xScale.domain(d3.extent(datas, d => new Date(d.created_at))) 
    _.yScale.domain([0, d3.max(datas, d => d.emotions.love)])

    _.yAxis = d3.axisLeft(_.yScale)
    .tickSize(width);
    _.xAxis = d3.axisBottom(_.xScale)
    .tickFormat(d3.timeFormat('%b %Y'));

    _.svg.select('.xAxis').transition().call(_.xAxis);
    _.svg.select('.yAxis').transition().call(_.yAxis);

     this.updateChart();
  };

  updateChart = () => {
    const _ = this;
    const datas = _.updatedDatas;
    const { width } = this;

    if (_.thumbs)
      _.thumbs.order()

    _.xScale.domain(d3.extent(datas, d => new Date(d.created_at))) 
    _.yScale.domain([0, d3.max(datas, d => d.emotions.love)])

    _.yAxis = d3.axisLeft(_.yScale)
    .tickSize(-width);
    _.xAxis = d3.axisBottom(_.xScale)
    .tickFormat(d3.timeFormat('%b %Y'))

    _.svg.select('.xAxis').transition().call(_.xAxis);
    _.svg.select('.yAxis').transition().call(_.yAxis);

    _.thumbs = _.groupThumbs.selectAll('.thumb').data(datas);

    _.thumbs
    .enter()
    .append('image')
    .attr('xlink:href', d => d.media.thumb)
    .attr("class","thumb")
    .attr('width', 0)
    .attr('height', 0)
    .attr("x", 0)
    .attr("y", d => _.yScale(d.emotions.love))
    .on("click", d => this.handleClickThumb(d))
    .transition()
    .duration(1000)
    .delay((d,i) => 20*i)
    .attr('width', 40)
    .attr('height', 40)
    .attr("x", d => _.xScale(new Date(d.created_at)))
    .attr("y", d => _.yScale(d.emotions.love))

    _.thumbs
    .attr('xlink:href', d => d.media.thumb)
    .transition()
    .duration(500)
    .delay((d,i) => 20*i)
    .attr("x", d => _.xScale(new Date(d.created_at)))
    .attr("y", d => _.yScale(d.emotions.love))
    .on('end', () => this.process = false);
  }

  componentDidMount() {
    this.drawChart();
  }
  
  handleClickThumb = (data) => {
    Modal.info({
      content: (
        <Router>
        <div>
          <PhotoContainer details={data}/>
        </div> 
        </Router>
      ),
      maskClosable: true,
      icon: null,
      onOk: null
    });
  }
        
  render(){
    return (
      <div className="histogramme">
        <header> Y : Like / X : Date </header>
        <svg ref={node => this.node = node}>
        </svg>
      </div>
    )

  }
}

export default Histogramme