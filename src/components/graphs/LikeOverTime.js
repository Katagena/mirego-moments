import React, { Component } from "react";
import { miregoService } from 'services/mirego.service';
import * as d3 from "d3";

class LikeOverTime extends Component {
  constructor(props){
    super(props);
    const { datas } = this.props

    this.margin = {top: 10, right: 30, bottom: 50, left: 20};
    this.width = 1300 - this.margin.left - this.margin.right;
    this.height = 500 - this.margin.top - this.margin.bottom;
    this.maxDate = datas[datas.length -1].created_at

    this.updatedDatas =  datas.reverse();
    this.svg = null;
    this.process = false;
  }

  loadDatas = () => {
    const _ = this;

    miregoService.get(null, _.maxDate).then(datas => {
      _.updatedDatas = [...datas.reverse(), ..._.updatedDatas, ];
      _.maxDate = _.updatedDatas[0].created_at
      _.updateChart();
    }).catch(error => console.debug(error));
  }

  handleClick = () => { 
    if (!this.process){
      this.loadDatas();
      this.process = true; 
    } 
  }
    
  drawChart = () => {
    const _ = this
    const datas = this.updatedDatas;
    const node = _.node
    const { margin, width, height } = this;

    _.svg = d3.select(node)
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

    _.svg.append('g').attr('class', 'xAxis').attr('transform', 'translate(0, ' + height+ ')');
    _.svg.append('g').attr('class', 'yAxis');
    _.groupLines = _.svg.append('g').attr('class', 'group-lines')
    _.groupDots = _.svg.append('g').attr('class', 'group-dots')

    _.xScale = d3.scaleTime()
    .range([0, width]);

    _.yScale = d3.scaleLinear()
    .range([height, 0]);

    _.xScale.domain(d3.extent(datas, d => new Date(d.created_at))) 
    _.yScale.domain([0, d3.max(datas, d => d.emotions.love)])

    _.yAxis = d3.axisLeft(_.yScale)
    .tickSize(width);
    _.xAxis = d3.axisBottom(_.xScale)
    .tickFormat(d3.timeFormat('%b %Y'));

    _.svg.select('.xAxis').transition().call(_.xAxis);
    _.svg.select('.yAxis').transition().call(_.yAxis);

     this.updateChart();
  };

  updateChart = () => {
    const _ = this;
    const datas = _.updatedDatas;
    const { width } = this;

    _.xScale.domain(d3.extent(datas, d => new Date(d.created_at))) 
    _.yScale.domain([0, d3.sum(datas, d => d.emotions.love)])

    let sum = 0;
    let dataSet = datas.map( d => {
      sum += d.emotions.love
      return {created_at: d.created_at, sum: sum}
    })

    if (_.path)
      _.path.remove()
    
    _.yAxis = d3.axisLeft(_.yScale)
    .tickSize(-width);
    _.xAxis = d3.axisBottom(_.xScale)
    .tickFormat(d3.timeFormat('%b %Y'))

    _.svg.select('.xAxis').transition().call(_.xAxis);
    _.svg.select('.yAxis').transition().call(_.yAxis);

    _.line = d3.line()
    .x(d => _.xScale( new Date(d.created_at))) 
    .y(d => _.yScale(d.sum))
    .curve(d3.curveMonotoneX)

    _.path = _.groupLines.append("path")
    .datum(dataSet) 
    .attr("class", "line") 
    .attr("d", _.line); 

    var totalLength = _.path.node().getTotalLength();

    _.path.attr("stroke-dasharray", totalLength + " " + totalLength)
      .attr("stroke-dashoffset", totalLength)
      .transition()
      .duration(2000)
      .ease(d3.easeLinear)
      .attr("stroke-dashoffset", 0)
      .on("end", () => this.process = false);

    _.dots = _.groupDots.selectAll(".dot")
    .data(dataSet)

    _.dots.enter()
    .append("circle") // Uses the enter().append() method
    .attr("class", "dot") // Assign a class for styling
    .attr("cx", d => _.xScale( new Date(d.created_at)))
    .attr("cy", d => _.yScale(d.sum))
    .attr("r", 5)

    _.dots.transition()
    .attr("cx", d => _.xScale( new Date(d.created_at)))
    .attr("cy", d => _.yScale(d.sum))
  }

  componentDidMount() {
    this.drawChart();
  }
        
  render(){
    return (
      <div className="like-over-time">
        <header> Like over date </header>
        <svg ref={node => this.node = node}>
        </svg>
      </div>
    )

  }
}

export default LikeOverTime