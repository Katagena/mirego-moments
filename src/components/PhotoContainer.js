import React, {Component} from "react";
import { Card, Avatar } from 'antd';
import moment from 'moment';
import LikeBox from './_parts/_LikeBox';
import { Link } from 'react-router-dom';
const { Meta } = Card;

class PhotoContainer extends Component{
  constructor(props) {
    super(props);
    this.state = {
      isActive: false
    }
  }

  componentDidMount() {
    setTimeout(() => this.setState({isActive: true}), 100)
  }

  render(){
    const { isList, details: photo } = this.props
    const { isActive } = this.state
    const content = 
      <Card 
        className={isActive ? "photo-container active" : "photo-container"}
        hoverable={isList ? true : false}
        bordered={false}
      >
        <header>
          <Meta
          title={photo.description}
          avatar={
            <Avatar
              shape="square"
              size={64}
              alt={photo.description}
              src={photo.user.avatar.thumb}
            />}
            description={`by ${photo.user.full_name} ${moment(photo.created_at).fromNow()}`}
          />
          <LikeBox id={photo.id} number={photo.emotions.love} liked={photo.liked}/>
        </header>
        <img className="photo" alt={photo.description} src={photo.media.xlarge} />
      </Card>

    return isList ? (
    <Link to={{ pathname: `/${photo.id}` }}>{content}</Link>
    ) : (
    <>{content}</>
    )
  }
}

export default PhotoContainer;