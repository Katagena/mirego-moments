import React, { Component } from 'react';
import { Form, Icon, Input, Button, message } from 'antd';
import { withCookies } from 'react-cookie';
import { loginService } from 'services/login.service';
import { withRouter } from 'react-router-dom';
var md5 = require('md5');

class FormLog extends Component {
  constructor(props) {
      super(props);
      this.state = {};
      const { register } = this.props
      const type = register ? 'Register' : 'Login'
      document.title = `Mirego - ${type}`;
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    const { cookies, history, register} = this.props;
    let values;
    // test des champs par antd
    this.props.form.validateFields((err, val) => {
      if (!err) {
        values = val;
      }else{
        return 0;
      }
    });
    if (values){
      try {
        const token = await loginService.log(values.username, md5(values.password), register)
        let d = new Date();
        d.setTime(d.getTime() + (60*60*1000));
        cookies.set("auth",{ 'id': token.id, 'username':token.username,'token':token.token }, { expires: d })
        message.success(`Welcome ${values.username} !`);
        history.push("/")
      } catch (error) {
          message.error('Wrong Login / Password');
      }
    }
  };

  render(){
    const { getFieldDecorator } = this.props.form;
    const { register } = this.props
    return ( 
      <>
        <Form onSubmit={this.handleSubmit} className="log-form">
        <Form.Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please enter your username!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Username"
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please enter your Password!' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
            />,
          )}
        </Form.Item>
        <Form.Item className="button-submit">
          <Button type="primary" htmlType="submit" className="button-submit">
            { register ? 'Register' : 'Login'}
          </Button>
        </Form.Item>
      </Form>
    </>
    )
  }
}

const WerappedRegistration = Form.create({ name: 'log' })(FormLog);
export default withRouter(withCookies(WerappedRegistration));