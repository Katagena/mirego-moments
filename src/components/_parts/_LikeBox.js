import React, {Component} from "react";
import { withCookies } from 'react-cookie';
import { Icon, Tooltip } from 'antd';
import { loginService } from 'services/login.service';
import { withRouter } from 'react-router-dom';

class LikeBox extends Component{
  constructor(props) {
    super(props);
    const {cookies} = props;
    this.state = {
        number: this.props.number,
        liked: this.props.liked,
        id: this.props.id,
        auth: cookies.get('auth')
    }
  }

  handleClick = async (event) => {
    const { history, cookies } = this.props

    event.preventDefault()
    const { auth } = this.state
    if (auth){
      try {
        await loginService.postLike(auth.id, this.state.id, auth.token)
        this.setState(prevState => {
          let number = prevState.liked ? -1 : 1;
          return {number: this.state.number + number,liked: !prevState.liked}
        })
      } catch (error) {
        if (error.result.status === 401){
          cookies.remove('auth')
          history.push("/login")
        }else{
          history.push("/error")
        }
      }
    }
  }

  render(){
    const { number, liked, auth } = this.state

    const content = <div className="like-box" onClick={this.handleClick}><span className="number">{number}</span><Icon type="heart" theme="twoTone" twoToneColor={liked ? "#eb2f96" : "#3B8DFE"}/> </div>   

    return auth ?
      <>{content} </>
      :
      <Tooltip placement="left" title="Log in to like !">
         {content}
      </Tooltip>
  }
}

export default withRouter(withCookies(LikeBox))