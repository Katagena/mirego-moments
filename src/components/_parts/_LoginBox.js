import React, {Component} from "react";
import { withCookies } from 'react-cookie';
import { Link } from 'react-router-dom';
import { Icon } from 'antd';

class LoginBox extends Component{
  constructor(props) {
    super(props);
    const {cookies} = props;
    this.state = {
        auth: cookies.get('auth')
    }
  }

  handleClick = (e) => {
    e.preventDefault();
    const { cookies } = this.props;
    cookies.remove('auth');
    window.location.reload()
  }

  render(){
    const { auth } = this.state;
    const content = auth ? <span> {auth.username} <Link to="#" onClick={this.handleClick}><Icon type="logout" /> Logout </Link></span> : <span><Link to="/login" ><Icon type="login" /> Login</Link>  <Link to="/register" ><Icon type="form" /> Register </Link> </span>
    return (
      <div className="login-box">{content}</div>
    )
  }
}

export default withCookies(LoginBox)