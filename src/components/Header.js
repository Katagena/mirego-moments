import React, { Component } from 'react';
import { Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';
import LoginBox from 'components/_parts/_LoginBox'
import { withRouter } from 'react-router-dom'

class Header extends Component {

  render(){
    const { breads } = this.props
     
    return ( 
      <header className="header">
        <div className="links">
        <div className="hidden-link"> <Link to="/stat">Statistiques</Link> </div>
        <LoginBox />
        </div>
        <div className="bread">
          <Breadcrumb separator=">">
          {breads.map((bread, index)=> ( 
            <Breadcrumb.Item key={index}>
              {bread.url !== '' ?<Link to={bread.url}>{bread.label}</Link> : bread.label}
            </Breadcrumb.Item>
          ))}
          </Breadcrumb>
        </div>
      </header>
    )
  }
}

export default withRouter(Header);