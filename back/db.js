var sqlite3 = require('sqlite3').verbose()
var md5 = require('md5')

const DBSOURCE = "db.sqlite"


const query = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      // Cannot open database
      console.error(err.message)
      throw err
    }else{
        console.log('Connected to the SQLite database.')
        query.run(`CREATE TABLE users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username text, 
            password text, 
            CONSTRAINT username_unique UNIQUE (username)
            )`,
        (err) => {
          if (err) {
            // Table already created
          }else{
            // Table just created, creating some rows
            var insert = 'INSERT INTO users (username, password) VALUES (?,?)'
            query.run(insert, ["admin",md5("admin")])
            query.run(insert, ["user", md5("user")])
          }
        });

        query.run(`CREATE TABLE likes (
              user_id INTEGER,
              photo_id text,  
              PRIMARY KEY (user_id, photo_id)
            )`,
        (err) => {
           
        });  
    }
});

const all = (sql, params, res) => {
  query.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({"error":err.message});
      return;
    }
    res.json(rows)
  });
}

const get = (sql, params, res) => {
  query.get(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({"error":err.message});
      return;
    }
    rows = rows ? rows : []
    res.json(rows)
  });
}

const sqlite = {
  query, all, get
}


module.exports = sqlite
