// Create express app
const express = require("express"),
cors = require('cors'),
app = express(),
bodyParser = require("body-parser"),
routes = require('./routes'),
HTTP_PORT = 8000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors({
  origin: '*',
  exposedHeaders: ['Content-Type','Accept', 'Authorization'],
}));

// console log
app.use((req, res, next)=> {
  console.log([req.method, req.url, req.body]);
  next();
});
app.use('/', routes);

// 404
app.use(function(req, res){
    res.status(404);
});

// invalide token
app.use((err, req, res) => {
	if (err.name === 'UnauthorizedError') {
		res.status(401).json({ message: 'Unauthorized. Invalid token!' });
	}
});

app.listen(HTTP_PORT, () => {
  console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});