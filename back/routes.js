const express = require('express'),
jwt = require('jsonwebtoken'),
jwt_authenticate = require('express-jwt'),
db = require("./db.js"),
router = express.Router(),
secret = 'sdfwefwefsda1fa65dv1a65v1a5v189wer564';

// GET
router.get("/api/likes", (req, res, next) => {
  var sql = "SELECT * FROM LIKES";
  var params = [];
  db.all(sql, params, res);
})
.get("/api/likes/count", (req, res, next) => {
  var sql = "SELECT photo_id AS id, count(*) AS like FROM likes GROUP BY photo_id";
  var params = [];
  db.all(sql, params, res);
})
.get("/api/likes/count/:photo_id", (req, res, next) => {
  var sql = "SELECT count(*) AS like FROM likes WHERE photo_id = ?";
  var params = [req.params.photo_id];
  db.get(sql, params, res)
})
.get("/api/likes/by_user/:user_id",jwt_authenticate({secret: secret}), (req, res, next) => {
  var sql = "SELECT * FROM likes WHERE user_id = ?";
  var params = [req.params.user_id];
  db.all(sql, params, res);
})
.get("/api/likes/by_user/:user_id/by_photo/:photo_id", jwt_authenticate({secret: secret}), (req, res, next) => {
  var sql = "SELECT count(*) AS like FROM likes WHERE user_id = ? AND photo_id = ?";
  var params = [req.params.user_id, req.params.photo_id];
  db.get(sql, params, res)
});

// POST 
router.post("/api/likes", jwt_authenticate({secret: secret}),(req, res, next) => {
  var sql ='SELECT count(*) AS count FROM likes WHERE user_id = ? AND photo_id = ?'
  var params =[req.body.user_id, req.body.photo_id]
  db.query.get(sql, params, function (err, row) {
      if (err){
          res.status(400).json({"error": err.message})
          return;
      }
      if (row.count === 1){
        sql ='DELETE FROM likes WHERE user_id= ? AND photo_id = ?'
      }else{
        sql ='INSERT INTO likes (user_id, photo_id) VALUES (?,?)'
      }
      db.get(sql, params, res)
  });
})
.post("/api/login", (req, res, next) => {
  var sql ='SELECT * FROM users WHERE username = ? AND password = ?'
  var params =[req.body.username.toLowerCase(), req.body.password]
  db.query.get(sql, params, function (err, user) {
    if (err){
        res.status(400).json({"error": err.message})
        return;
    }
    if (!user){
      res.status(404).json({"error": "Incorrect username or password"})
      return;
    }else{
      const payload = {user} ;
      const token = jwt.sign(payload, secret, { expiresIn: 3600 });
      res.json({...payload.user, 'token':token})
    }
  });
})
.post("/api/register", (req, res, next) => {
  var sql ='INSERT INTO users (username, password) VALUES (?, ?)'
  var params =[req.body.username.toLowerCase(), req.body.password]
  db.query.get(sql, params, function (err, user) {
    if (err){
        res.status(400).json({"error": err.message})
        return;
    }
    var sql ='SELECT * FROM users WHERE username = ? AND password = ?'
    var params =[req.body.username.toLowerCase(), req.body.password]
    db.query.get(sql, params, function (err, user) {
      if (err){
          res.status(400).json({"error": err.message})
          return;
      }
      if (!user){
        res.status(404).json({"error": "Incorrect username or password"})
        return;
      }else{
        const payload = {user} ;
        const token = jwt.sign(payload, secret, { expiresIn: 3600 });
        res.json({...payload.user, 'token':token})
      }
    });
  });
});

module.exports = router;